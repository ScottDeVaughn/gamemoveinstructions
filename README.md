# Moving games to your HDD

Alright ya dingus, let's start this shit. We're going to be completing the following tasks with minimal bitching:

1. Set your Ext HDD's Drive Letter/Path
2. Set up Steam
3. Set up Blizzard
4. Set up Uplay
5. Set up Origin

## 1. Set your Ext HDD's Drive Letter/Path

<p>OK, in order to avoid any Drive Letter mis-assigning shenanigans, we want to give it a higher letter than just D or E. After plugging in your Ext HDD (ya know what, we're just going to call it EHD from now on), do the following:

* Open up `File Explorer` and right-click `This PC` in the left pane
* Click `Manage`
* After, `Computer Management` opens, click on `Disk Management` in the left pane under `Storage`
* There should be rows of squares and rectangles. Locate the row that is your EHD.
* For your EHD row, right-click the rectangle with the blue line
* Click `Change Drive Letter and Paths...`
    * If it is pre-filled with `C:` , congratulations you found the wrong drive! Go back and find the correct drive row.
* Click on the Drive letter and then click `Change`
* Select `Assign the following drive letter:` if it isn't already
* Select another letter(preferrably one greater than `E`)
    * I have mine set to `G` and I named my EHD `Games`. `G` for `Games`, get it???
        * Do you get it?
* Click `OK`
* Click `OK` again
* Exit `Computer Management`
* Verify that your EHD is assign to the new letter by going to `This PC` and looking at what drive letter is assigned to it.
* Open your EHD directory (just double-click that shit)
* Make the following new folders:
    * Blizzard
    * Origin
    * Uplay
        * You only need to make this if you have Uplay installed. 
* We won't make a `Steam` folder because we'll be copying the whole thing over later.
</p>

## 2. Set up Steam

<p>Now that we know our drive won't be fucked over by windows when you plug in something else like a thumb-drive, we can tell Steam where to find shit. Open Steam and follow these steps:

* Click the `Steam` menu item in the top left of the menu
* Click `Downloads` in the left pane
* Click the `STEAM LIBRARY FOLDERS` button at the top of the window
* Click `ADD LIBRARY FOLDER`
* At the very top of the window, click on the select list and select your EHD letter
* Create a new folder named `Steam`
* Click `Select`
    * If you fuck it up, just right-click the entry you want to remove and select `Remove Library Folder`
* Click `Close`
* Click `OK`
</p>

<p>You can either start transferring stuff now, or wait until later. If you want to do it now, follow these steps:</p>

* Open TWO `File Explorer`s
    * In window A, navigate to your EHD
    * In window B, navigate to `C:\Program Files (x86)`
        * If you don't see a `Steam` folder here, try `C:\Program Files`
            * If you still don't see a `Steam` folder, where the fuck did you install it?
* Drag the contents of the `Steam` folder from window B to window A.
    * I know you know how to drag
* This shit will take a while so make a drink I guess

<p>After it has finished transferring, we'll need to delete the games from your SSD because that shit takes up space. 

* Once again open a `File Explorer` 
* Navigate to `C:\Program Files (x86)\Steam\steamapps\common`
    * as before, use `Program Files` if your `Steam` folder doesn't exist in `Program Files (x86)`
* Delete errything in this folder.
</p>

## 3. Set up Blizzard

<p>Do these things:

* Open `Battle.net` app
* Click on a game that's already installed
* Click `Options` in the sub-pane at the top left
* Select `Game Settings`
* Click `Game Install/Update` in the left pane of the new window if it isn't already selected
    * Make note of the current install locations for your games
        * we'll refer to this path as `Current Blizzard Path` or `CBP`
* Click `Change` next to the `DEFAULT INSTALL DIRECTORY`
* In the selection window, navigate to your EHD and select the `Blizzard` folder
* Click `Done`
</p>

<p>Time to transfer your games (or later if you don't want to do it now).

* Open TWO `File Explorer`s
* In window A, navigate to your EHD and open the `Blizzard` folder
* In window B, navigate to your `CBP` (I think it's probably `C:\Program Files (x86)\Battle.net`)
* Drag the game folders from window B to window A
* Wait for that shit to transfer
* After that completes open the `Blizzard` again
* Open `Battle.net` app
* Click on a game that's already installed
* Click `Options` in the sub-pane at the top left
* Select `Game Settings`
* Click `Game Install/Update`
* For every game, click `Use a Different Folder` and select the `EHD > Blizzard > gameName` folder 
* Click `Done`
* Delete StarCraft 2 folder from your SSD. 
* Attempt to play StarCraft 2 
    * If it plays, delete the rest of the Blizzard games from you SSD
    * If it does not play, let me know because I'll have to do other shit. But it should work anyway
</p>

## 4. Origin

<p>OK. Origin is a pile of shit and this might be more difficult.

* Open `Origin`
* Click `My Game Library` in the left pane
* You'll need to complete these next steps for all games:
    * Right-click the games
    * Select `Move Game`
    * Select the `EHD > Origin` folder
    * The game should transfer on its own
    * After the game is transferred, Origin then has to verify that the files were transferred
        * I'm not sure if you can try the next game. To be safe, just do it one at a time.
            * Yeah, I know
    * Repeat for the other games

In order to get Origin to install games to the new location, do the following:

* Click the `Origin` menu item in the top left corner of `Origin`
* Click `Application Settings` 
* In the main window pane, click `INSTALLS & SAVES`
* Click `Change Folder`
* Select the `EHD > Origin` folder
</p>

## 5. Uplay
<p>Now we do basically the same shit for Uplay. Steps:

* If any of your games are linked with Steam, move Steam first. (IDK why, the thing online said it.)
* Open the `Uplay` application
* Click the `Games` menu link up at the top
* Click on a game tile
* On that game's page, click `Properties` link on the left pane.
* Note the install location under the `Local files` section
* Open two `File Explorer`s
* In window A, navigate to your EHD and open the `Uplay` folder
* In window B, navigate to your the install location of your Uplay game
* Drag the folder from window B to window A
* Back in the `Uplay` app, under that game's page, click `Open folder` in the `Local files` section
* Select the game folder under `EHD > Uplay > gameName`
* Repeat for the rest of your Uplay games

</p>
